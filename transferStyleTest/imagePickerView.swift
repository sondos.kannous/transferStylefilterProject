//
//  imagePicker.swift
//  SwiftUIlibrary App
//
//  Created by CodersDc on 8/2/23.
//

import SwiftUI
import CoreML


struct imagePickerView: View {
    @State var showImagePicker: Bool = false
    @State var showselectPicker: Bool = false
    @State var image: Image? = nil
    @State var isCamera = false
   
        var body: some View {
            ZStack {
                VStack {
                    Button(action: {
                        self.showselectPicker.toggle()
                    }) {
                        Text("Show image picker")
                    }
                    image?.resizable().frame(width: 200, height: 200)
                }
                .sheet(isPresented: $showselectPicker  ){
                    ZStack{
                      Color.gray.edgesIgnoringSafeArea(.all)
                        
                    VStack{
                    Spacer()
                    if(showImagePicker){
                        FromGallery(image: $image,showImagePicker: $showImagePicker, fromCmera: $isCamera)
                    }
                    else {
                  
                        VStack{
                            Text("Image Picker").onTapGesture{
                                showImagePicker = true
                                isCamera = false
                            }
                        Divider()
                        
                            Text("camera").onTapGesture{
                                showImagePicker = true
                                isCamera = true
                            }
                            
                        
                            
                        }.frame(maxWidth:.infinity, maxHeight: 200).background(Rectangle().fill(.white))}
                        
                    }
                   
                
                    }.edgesIgnoringSafeArea(.all)
                }
                        
                    //to prefix sheet height and width use .presentationDetents([.height(200)]) in content of sheet

                }
            }
 



        }

    
    struct FromGallery: View {
        @Binding var image : Image?
        @Binding var showImagePicker :Bool
        @Binding var fromCmera : Bool
        let styleTransferModel = try! StyleTransferTest_2(configuration: MLModelConfiguration())
        var body: some View {
            ImagePicker(sourceType: fromCmera ? .camera : .photoLibrary) { uiImage in
                let fixedImage = uiImage.fixOrientation() // Fix orientation here
                           
                           if let styledImage = applyStyleTransfer(to: fixedImage) {
                               self.image = Image(uiImage: styledImage)
                           } else {
                               // Handle style transfer error
                           }
                           showImagePicker = false
                       }
            
            
        }
      
        func applyStyleTransfer(to image: UIImage) -> UIImage? {
            guard let pixelBuffer = image.pixelBuffer(width: 512, height: 512) else {
                return nil
            }
            
            do {
                let input = StyleTransferTest_2Input(image: pixelBuffer)
                let output = try styleTransferModel.prediction(input: input)
                
                let stylizedPixelBuffer = output.stylizedImage
                
                // Directly convert the CVPixelBuffer to a UIImage
                let ciImage = CIImage(cvPixelBuffer: stylizedPixelBuffer)
                guard let cgImage = CIContext().createCGImage(ciImage, from: ciImage.extent) else {
                    return nil
                }
                
                let outputImage = UIImage(cgImage: cgImage)
                return outputImage
            } catch {
                print("Style transfer error: \(error)")
                return nil
            }
        }
    }


extension UIImage {
    func fixOrientation() -> UIImage {
        guard let cgImage = cgImage else {
            return self
        }
        
        if imageOrientation == .up {
            return self
        }
        
        var transform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: .pi)
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: .pi / 2)
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: -(.pi / 2))
            
        default:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        default:
            break
        }
        
        guard let context = CGContext(
            data: nil,
            width: Int(size.width),
            height: Int(size.height),
            bitsPerComponent: cgImage.bitsPerComponent,
            bytesPerRow: 0,
            space: cgImage.colorSpace!,
            bitmapInfo: cgImage.bitmapInfo.rawValue
        ) else {
            return self
        }
        
        context.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            context.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            
        default:
            context.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        }
        
        if let fixedImage = context.makeImage() {
            return UIImage(cgImage: fixedImage)
        } else {
            return self
        }
    }


    func pixelBuffer(width: Int, height: Int) -> CVPixelBuffer? {
        let attrs: [String: Any] = [
            kCVPixelBufferCGImageCompatibilityKey as String: kCFBooleanTrue,
            kCVPixelBufferCGBitmapContextCompatibilityKey as String: kCFBooleanTrue
        ]
        
        var pixelBuffer: CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, width, height, kCVPixelFormatType_32ARGB, attrs as CFDictionary, &pixelBuffer)
        
        guard status == kCVReturnSuccess, let buffer = pixelBuffer else {
            return nil
        }
        
        CVPixelBufferLockBaseAddress(buffer, [])
        defer { CVPixelBufferUnlockBaseAddress(buffer, []) }
        
        let context = CGContext(
            data: CVPixelBufferGetBaseAddress(buffer),
            width: width,
            height: height,
            bitsPerComponent: 8,
            bytesPerRow: CVPixelBufferGetBytesPerRow(buffer),
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue
        )
        
        guard let ctx = context else {
            return nil
        }
        
        ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: width, height: height))
        return pixelBuffer
    }
}
