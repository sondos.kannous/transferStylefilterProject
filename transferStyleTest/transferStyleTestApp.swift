//
//  transferStyleTestApp.swift
//  transferStyleTest
//
//  Created by CodersDc on 8/16/23.
//

import SwiftUI

@main
struct transferStyleTestApp: App {
    var body: some Scene {
        WindowGroup {
            StyleTransferApp()
        }
    }
}
